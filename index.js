const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3000;


mongoose.connect("mongodb+srv://jeff0818:Ryanjefferson013@batch224.1dappv1.mongodb.net/s35Activity?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }

);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB"));


const userSchema = new mongoose.Schema({
    name: String,
    password: String,
    status: {
        type: String,
        default: "pending"
    }
});

const User = mongoose.model("User", userSchema);



app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.post("/signup", (req, res) => {
    User.findOne({ name: req.body.name }, (err, result) => {

        if (result != null && result.name == req.body.name) {

            return res.send("Duplicate user found!")

        } else {
            let newUser = new User({
                name: req.body.name,
                password: req.body.password
            });
            newUser.save((saveErr, savedUser) => {

                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New user created");
                }
            })
        }
    })
});

app.get("/users", (req, res) => {

    User.find({}, (err, result) => {

        if (err) {
            return console.log("err");
        }
        else {
            return res.status(200).json({
                data: result
            })
        }
    })


});











app.listen(port, () => console.log(`Server running at port ${port}`));